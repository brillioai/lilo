'use strict';
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const path = require('path');
const http = require('http');
const config = require('./config');
const cors = require('cors');

const app = express();
const server = http.createServer(app);
const port = process.env.PORT;

const logs = require("./utils/logs");
logs(app);

let options = {
    inflate: true,
    limit: '100kb',
    type: 'application/json'
};

app.use(cors());

// Un comment below code to restrict to white-listed URLs, once you got whitelistURLs in config file

// var whitelist = config.whitelistURLs;
// var corsOptions = {
//   origin: function (origin, callback) {
//     if (whitelist.indexOf(origin) !== -1) {
//       callback(null, true)
//     } else {
//       callback(new Error('Not allowed by CORS'))
//     }
//   }
// }

// app.use(cors(corsOptions));

app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.raw(options));

app.use(express.static('./docs'));

require('./routes/chatbot-routes')(app);

app.get('*', function (req, res, next) {
    let err = new Error(`${req.ip} tried to reach ${req.originalUrl}`);
    err.statusCode = 404;
    next(err);
});

server.listen(config.port, config.server, () => {
    console.log(`Lilo Pitch Dashboard API Server(Express) started on ${config.server}, listening on port ${config.port} in ${app.get('env')} mode`);
});