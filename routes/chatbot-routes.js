'use strict';
const chatbotController = require('../controllers/chatbotController');
const routeUtil = require('../utils/routeUtil');

module.exports = function (app) {
    app.get('/', (req, res) => {
        return routeUtil(res, 'Lilo Pitch Dashboard APIs', 200);
    });

    app.post('/chatbot', chatbotRequestHandller);

    //      All handller methods

    function chatbotRequestHandller(req, res) {
        var token = req.headers.authorization;
        chatbotController.getDashboardData(req.body, token).then(data => {
            routeUtil(res, data, 200);
        }).catch(err => {
            routeUtil(res, err.message, err.statusCode);
        })
    }

};

