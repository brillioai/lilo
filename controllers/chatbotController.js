'use strict'
const intentHandlers = require("../utils/intentHandlers");
const utils = require("../utils/utilFunctions");
const errorMessages = require("../utils/commands").errorMessages;
const defaultSuggestions = require("../utils/commands").defaultSuggestions;
const controller = {};


controller.getDashboardData = async (body, token) => {
    var result = new Promise((resolve, reject) => {

        // Initialize all reseponse fields
        var botResponse = {
            textResponse: [],
            voiceResponse: [],
            suggestion: {},
            sessionID: ""
        }

        // Check for all request parameters
        if (typeof body.username == "undefined" || typeof body.userUtterance == "undefined" || typeof body.sessionID == "undefined" ||
            typeof body.firstMessage == "undefined") {
            botResponse.textResponse.push(errorMessages.missingParams);
            botResponse.voiceResponse = botResponse.textResponse;
            resolve(botResponse);
        }

        // Handle first message

        if (body.firstMessage) {
            // Random session ID generation for new conversation 
            botResponse.sessionID = "" + Math.floor(1000000000 + Math.random() * 9999999999);
            if (body.userUtterance) {
                // If user is sending 1st message and asking for something, Bot will say hi before processing further
                botResponse.textResponse.push(`Hello ${body.username}!`);
            }
            else {
                // If Bot is simply invoked and no questions asked trigger coversarion dialog
                body.userUtterance = "bot invoked";
            }
        }
        // Not a first message and no user utterance
        else if (!body.userUtterance) {
            botResponse.textResponse.push(errorMessages.emptyRequest);
            botResponse.voiceResponse = botResponse.textResponse;
            resolve(botResponse);
        }

        // Handle first session ID

        // For ongoing conversations set session ID from request Object
        if (!botResponse.sessionID && body.sessionID) {
            botResponse.sessionID = body.sessionID;
        }
        // In ongoing conversations if session ID not sent from user in request Object
        else if (!botResponse.sessionID) {
            botResponse.textResponse.push(errorMessages.InvalidSessionID);
            botResponse.voiceResponse = botResponse.textResponse;
            resolve(botResponse);
        }

        // NPL 
        utils.apiCall(body.userUtterance, botResponse.sessionID)
            .then(data => {
                // Check user access for expenses
                if (!utils.checkPrivilegedUser(body.userRole) && (data["result"]["metadata"]["intentName"].indexOf("cloud-services") >= 0)) {
                    botResponse.textResponse.push(errorMessages.UserNotAuthorised);
                    botResponse.voiceResponse = botResponse.textResponse;
                    resolve(botResponse);
                }
                // OnConversation Update
                else if (data["result"]["metadata"]["intentName"] == "chatbot-invoked") {
                    intentHandlers.botInvoked(body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // Conversation Update fallbacks
                else if ((data["result"]["metadata"]["intentName"] == "chatbot-invoked - no" || data["result"]["metadata"]["intentName"] == "chatbot-invoked - yes") && data["result"]["contexts"].length >= 2) {
                    intentHandlers.botInvokedFollowUp(body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // Default fallback
                else if (data["result"]["metadata"]["intentName"] == "Default Welcome Intent") {
                    intentHandlers.defaultWelcomeIntent(body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    })
                }
                // Dashboard Summary 
                else if (data["result"]["metadata"]["intentName"] == "chatbot-dashboard-summary") {
                    intentHandlers.dashboardSummary(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // Warehouse locations count
                //            All locations, locations by country, locations by state
                else if (data["result"]["metadata"]["intentName"] == "chatbot-warehouse-locations-count") {
                    intentHandlers.warehouseLocationsCount(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // P1 tickets count
                else if (data["result"]["metadata"]["intentName"] == "chatbot-warehouse-tickets-count") {
                    intentHandlers.warehouseTicketsCount(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // Warehouse tickets difference
                else if (data["result"]["metadata"]["intentName"] == "chatbot-warehouse-tickets-difference") {
                    intentHandlers.warehouseTicketsDifference(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // Resolved Tickets
                else if (data["result"]["metadata"]["intentName"] == "chatbot-warehouse-resolvedTickets") {
                    intentHandlers.warehouseResolvedtickets(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // warehouse-category-distribution-statistics
                else if (data["result"]["metadata"]["intentName"] == "chatbot-warehouse-category-distribution-statistics") {
                    intentHandlers.chatbotWarehouseCategoryDistributionStatistics(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // warehouse-monthlyTickets-byLocationID
                else if (data["result"]["metadata"]["intentName"] == "chatbot-warehouse-monthlyTickets") {
                    intentHandlers.warehouseMonthlyTickets(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // warehouse-monthof-highest-tickets
                else if (data["result"]["metadata"]["intentName"] == "chatbot-warehouse-monthof-highest-tickets") {
                    intentHandlers.warehouseMonthofHighestTickets(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // warehouse-contributing-factor
                else if (data["result"]["metadata"]["intentName"] == "chatbot-warehouse-contributing-factor") {
                    intentHandlers.warehouseContributingFactor(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }

                // chatbot-warehouse-report
                else if (data["result"]["metadata"]["intentName"] == "chatbot-warehouse-report") {
                    intentHandlers.chatbotWarehouseReport(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // chatbot-warehouse-source-distribution-statistics
                else if (data["result"]["metadata"]["intentName"] == "chatbot-warehouse-source-distribution-statistics") {
                    intentHandlers.chatbotWarehouseSourceDistributionStatistics(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // chatbot-warehouse-resolution-distribution-statistics
                else if (data["result"]["metadata"]["intentName"] == "chatbot-warehouse-resolution-distribution-statistics") {
                    intentHandlers.chatbotWarehouseResolutionDistributionStatistics(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // chatbot-warehouse-manager-details
                else if (data["result"]["metadata"]["intentName"] == "chatbot-warehouse-manager-details") {
                    intentHandlers.chatbotWarehouseManagerDetails(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // chatbot-warehouse-bussiness-impact
                else if (data["result"]["metadata"]["intentName"] == "chatbot-warehouse-bussiness-impact") {
                    intentHandlers.chatbotWarehouseBussinessImpact(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // chatbot-warehouse-outage-window
                else if (data["result"]["metadata"]["intentName"] == "chatbot-warehouse-outage-window") {
                    intentHandlers.chatbotWarehouseOutageWindow(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // cloud-services-cost-by-services
                else if (data["result"]["metadata"]["intentName"] == "chatbot-cloud-services-cost-by-services") {
                    intentHandlers.cloudServicesCostByServices(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // cloud-services-cost-by-accounts
                else if (data["result"]["metadata"]["intentName"] == "chatbot-cloud-services-cost-by-accounts") {
                    intentHandlers.cloudServicesCostByAccounts(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // cloud-services-cost-by-locations
                else if (data["result"]["metadata"]["intentName"] == "chatbot-cloud-services-cost-by-locations") {
                    intentHandlers.cloudServicesCostByLocations(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // cloud-services-cost-by-environment
                else if (data["result"]["metadata"]["intentName"] == "chatbot-cloud-services-cost-by-environment") {
                    intentHandlers.cloudServicesCostByEnvTag(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // cloud-services-cost-by-resource-group
                else if (data["result"]["metadata"]["intentName"] == "chatbot-cloud-services-cost-by-resource-group") {
                    intentHandlers.cloudServicesCostByResourceGroup(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // cloud-services-total-cost
                else if (data["result"]["metadata"]["intentName"] == "chatbot-cloud-services-total-cost") {
                    intentHandlers.cloudServicesTotalCost(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // cloud-services-instances-count
                else if (data["result"]["metadata"]["intentName"] == "chatbot-cloud-services-instances-count") {
                    intentHandlers.cloudServicesInstancesCount(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // cloud-services-top-spent
                else if (data["result"]["metadata"]["intentName"] == "chatbot-cloud-services-top-spent") {
                    intentHandlers.cloudServicesTopSpent(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // cloud-services-expense-report
                else if (data["result"]["metadata"]["intentName"] == "chatbot-cloud-services-expense-report") {
                    intentHandlers.chatbotCloudServicesExpenseReport(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // chatbot-help
                else if (data["result"]["metadata"]["intentName"] == "chatbot-help") {
                    intentHandlers.chatbotHelp(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                 // chatbot-about
                 else if (data["result"]["metadata"]["intentName"] == "chatbot-about" || data["result"]["metadata"]["intentName"] == "smalltalk.agent.name") {
                    intentHandlers.chatbotAbout(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                  // chatbot-about
                  else if (data["result"]["metadata"]["intentName"] == "Default Fallback Intent") {
                    intentHandlers.chatDefaultFallback(data, body, botResponse, token).then(data => {
                        resolve(data);
                    }).catch(err => {
                        reject(err);
                    });
                }
                // Smalltalk
                else {
                    botResponse.textResponse.push(data["result"]["fulfillment"]["speech"]);
                    botResponse.suggestion = {}
                    botResponse.voiceResponse = botResponse.textResponse;
                    resolve(botResponse);
                }
            })
            .catch(err => {
                reject(err);
            })
    });
    var final = await result;
    if(final.voiceResponse) {
        final.voiceResponse.forEach((element,i) => {
            final.voiceResponse[i] = element.replace(/AWS/g,"Amazon Web Services")
    }); 
}
    return final;
}

module.exports = controller;