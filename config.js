'use strict'

const config = {};

config.port = process.env.PORT || 9000;
config.server = process.env.SERVER || '0.0.0.0';
config.whitelistURLs = [];
config.privilegedUserGroup = ["cxogroup"];
config.nlpKey = "f0012eeddf814dca9927ae810a133e76";
// config.dataAPIURL = "http://18.208.139.182:8080/dev-api/PITCH";
config.dataAPIURL = "http://18.208.139.182:8080/dev-api/PITCH_BOT";
config.prodAPIs = false
config.cloudServices = ["azure","aws"];

module.exports = config;