const utils = require("./utilFunctions");

const suggestion_noncxo = ["Summary Report", "Monthly Ticket Count", "Warehouse summary", "Resolved Tickets", "P1 Ticket Status"];
const suggestion_cxo = ["Summary Report", "Cloud Expense Report", "Monthly Ticket Count", "P1 Ticket Status", "Source Distribution", "Resolution Distribution", "Category Distribution"];

module.exports.defaultWelcomeCommands = (body) => {
    return {
        message: `Hello! Lilo here. I am here to assist you with information around warehouses and cloud expenses. Say help to know more about the things you can ask me.`,
        suggestion: {
            "message": "",
            "values": suggestionsValues((body.userRole))
        },
        voiceMessage: "Hello! Lilo here. I can help you with warehouse and cloud expense information. Say help or tap on the suggestions below to get started."
    }

}

module.exports.botInvokedCommands = (body) => {
    return {
        message: `Hi ${body.username}!`,
        suggestion: {
            "message": "Would you like me to read out the messages?",
            "values": ["Yes", "No"]
        },
        voiceMessage :`Hi ${body.username}`
    }
}

module.exports.botInvokedFollowUpCommands = (body) => {
    return {
        message: `I can summarize the dashboard for you, fetch warehouse statistics and give details about cloud expenses. You can say something like “Give me the dashboard summary”, “Show me the monthly tickets details”, “Cloud expense report” etc.`,
        suggestion: {
            "message": "Alternatively, you can choose one of the options below to get started.",
            "values": suggestionsValues((body.userRole))
        },
        voiceMessage: "I can help you with dashboard summary, warehouse statistics and cloud expense information."
    }
}

module.exports.helpCommands = (body) => {
    return {
        message: `I can help you with dashboard summary, warehouse statistics and cloud expense information. You can try some of the following queries : <br>\n
        “Give me the dashboard summary”,<br>\n 
        “Let me know the monthly ticket details”,<br>\n 
        “Top spent resource location”,<br>\n
        “Give me the number of tickets at USA/UK” 
        “Give me the statistics for source distribution”,<br>\n 
        “Resolution distribution overview”,<br>\n 
        “How is the category distribution looking like?”,<br>\n 
        “State cloud services cost by resource group”,<br>\n 
        “What’s the total cost of cloud services”,<br>\n 
        “How does cloud services cost vary with environments ”`,
        suggestion: {
            "message": "Alternatively, you can choose one of the options below to get started.",
            "values": suggestionsValues((body.userRole))
        },
        voiceMessage: "I can provide you dashboard summary, cloud expense reports and much more."
    }
}

module.exports.aboutCommands = (body) => {
    return {
        message: `I am Lilo, your in-app assistant. I can assist you with dashboard summary, warehouse statistics and cloud expense information.`,
        suggestion: {
            "message": "Alternatively, you can choose one of the options below to get started.",
            "values": suggestionsValues((body.userRole))
        },
        voiceMessage: "I am Lilo. I can help you with I can assist you with dashboard summary, warehouse statistics and cloud expense information."
    }
}

module.exports.defaultFallbackCommands = (data, body) => {
    return {
        message: data["result"]["fulfillment"]["speech"],
        suggestion: {
            "message": "",
            "values": suggestionsValues((body.userRole))
        },
        voiceMessage: data["result"]["fulfillment"]["speech"]
    }
}

module.exports.warehouseLocationsCountCommands = (allWarehouses, state) => {
    return {
        messagefoundWithState: `There are ${allWarehouses.length} warehouses in ${state}`,
        messagefoundWithoutState: `There are ${allWarehouses.length} warehouses.`,
        messageNotfoundWithState: `No warehouses found in ${state}.`,
        messageNotfoundWithoutState: `No warehouses found.`
    }
}

module.exports.warehouseTicketsCountCommands = (allTickets, state) => {
    var ticket = "tickets";
    if (allTickets.length == 1) {
        ticket = "ticket";
    }
    return {
        ticketsfoundWithState: `Total of ${allTickets.length} ${ticket} raised in ${state}`,
        ticketsfoundWithoutState: `Total ${allTickets.length} ${ticket} raised.`,
        ticketsNotfoundWithState: `No tickets were raised from ${state}.`,
        ticketsNotfoundWithoutState: `No tickets were raised.`
    }
}

module.exports.dashboardSummaryCommands = (allWarehousesInUSA, allWarehousesInUK, allTicketsInUSA, allTicketsInUK) => {
    return {
        summary1: `In both USA and UK there haven't been any issues registered.`,
        summary2: `As of today, out of ${allWarehousesInUSA.length} warehouses in USA, ${allTicketsInUSA.length} of them have P1 tickets. In UK, there haven't been any issues so far.`,
        summary3: `As of today, there haven't been any issues in USA. In UK, out of ${allWarehousesInUK.length} warehouses ${allTicketsInUK.length} of them have P1 tickets.`,
        summary4: `As of today, out of ${allWarehousesInUSA.length} warehouses in USA, ${allTicketsInUSA.length} of them have P1 tickets. In UK, out of ${allWarehousesInUK.length} warehouses ${allTicketsInUK.length} of them have P1 tickets.`,
        summary5: `No dashboard summary found`
    }
}

module.exports.errorMessages = {
    "emptyRequest": "Oops! Something went wrong. Try logging into the app once again.",
    "InvalidSessionID": "Oops! Something went wrong. Try logging into the app once again.",
    "UserNotAuthorised": "Sorry! I am not permitted to answer this at the moment.",
    "missingParams": "Oops! Something went wrong. Try logging into the app once again."
}

module.exports.defaultSuggestions = {
    "message": "I can help you with insights on warehouse operations and cloud expenses.",
    "values": ["Summary Report", "Cloud Expense Report"]
}

function suggestionsValues(userRole) {
    if (utils.checkPrivilegedUser(userRole)) {
        return suggestion_cxo
    }
    else {
        return suggestion_noncxo
    }
}