const intentHandlers = {}
const utils = require("../utils/utilFunctions");
const cities = require("./city");
const defaultSuggestions = require("./commands").defaultSuggestions;
const config = require("../config");
var dataAPI = require('./dataAPICalls_Dev');
const suggestion_noncxo = ["Summary Report", "Monthly Ticket Count", "Warehouse summary", "Resolved Tickets", "P1 Ticket Status"];
const suggestion_cxo = ["Summary Report", "Cloud Expense Report", "Monthly Ticket Count", "P1 Ticket Status", "Source Distribution", "Resolution Distribution", "Category Distribution"];

// if (config.prodAPIs) {
//     var dataAPI = require('./dataAPICalls_Dev');  
// }


intentHandlers.botInvoked = async (body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        var commands = {};
        commands = require("./commands").botInvokedCommands(body);
        botResponse.textResponse.push(commands.message);
        botResponse.voiceResponse.push(commands.voiceMessage);
        botResponse.suggestion = commands.suggestion;
        resolve(botResponse);
    })
    var final = await result;
    return final;
}

intentHandlers.botInvokedFollowUp = async (body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        var commands = {};
        commands = require("./commands").botInvokedFollowUpCommands(body);
        botResponse.textResponse.push(commands.message);
        botResponse.voiceResponse.push(commands.voiceMessage);
        botResponse.suggestion = commands.suggestion;
        resolve(botResponse);
    })
    var final = await result;
    return final;
}

intentHandlers.defaultWelcomeIntent = async (body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        var welcome = [], commands = {};
        commands = require("./commands").defaultWelcomeCommands(body);
        botResponse.textResponse.push(commands.message);
        botResponse.voiceResponse.push(commands.voiceMessage);
        botResponse.suggestion = commands.suggestion;
        resolve(botResponse);
    })
    var final = await result;
    return final;
}


intentHandlers.chatbotHelp = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        var welcome = [], commands = {};
        commands = require("./commands").helpCommands(body);
        botResponse.textResponse.push(commands.message);
        botResponse.voiceResponse.push(commands.voiceMessage);
        botResponse.suggestion = commands.suggestion;
        resolve(botResponse);
    })
    var final = await result;
    return final;
}

intentHandlers.chatbotAbout = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        var welcome = [], commands = {};
        commands = require("./commands").aboutCommands(body);
        botResponse.textResponse.push(commands.message);
        botResponse.voiceResponse.push(commands.voiceMessage);
        botResponse.suggestion = commands.suggestion;
        resolve(botResponse);
    })
    var final = await result;
    return final;
}

intentHandlers.chatDefaultFallback = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        var welcome = [], commands = {};
        commands = require("./commands").defaultFallbackCommands(data, body);
        botResponse.textResponse.push(commands.message);
        botResponse.voiceResponse.push(commands.voiceMessage);
        botResponse.suggestion = commands.suggestion;
        resolve(botResponse);
    })
    var final = await result;
    return final;
}

intentHandlers.warehouseLocationsCount = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        if (data["result"]["parameters"]["geo-state"].length) {
            var res = "";
            dataAPI.generalAPICall({ "cmd_id": "get_warehouse" }, token)
                .then(loc => {
                    var total_warehouses = 0;
                    data["result"]["parameters"]["geo-state"].forEach(x => {

                        var arr = [];
                        var abb = require("../utils/states").getAbrrevaition(x);
                        Object.keys(loc).forEach(l => {
                            arr = arr.concat(loc[l].locations);
                        })
                        var allwarehouses = arr.filter(l => {
                            if (l.location_id.indexOf(abb + ",") >= 0) { return l; }
                        });
                        total_warehouses = total_warehouses + allwarehouses.length;
                    });
                    var wdata = total_warehouses == 1 ? "There is one warehouse" : `There are ${total_warehouses} warehouses`
                    var finalData = total_warehouses == 0 ? "There are no warehouses" : wdata
                    res = `${finalData} in ${data["result"]["parameters"]["geo-state"].join(", ")}`
                    botResponse.textResponse.push(res);
                    botResponse.voiceResponse = botResponse.textResponse;
                    botResponse.suggestion = {
                        "message": "",
                        "values": ["P1 Ticket Status", "Monthly Ticket Count", "Source Distribution", "Resolution Distribution", "Category Distribution"]
                    }
                    resolve(botResponse);
                })
        }
        else {
            dataAPI.generalAPICall({ "cmd_id": "get_warehouse_summary" }, token)
                .then(locationData => {
                    var res = "", allLocations = [];
                    if (data["result"]["parameters"]["geo-country"].length > 0) {
                        data["result"]["parameters"]["geo-country"].forEach((c, i) => {
                            allLocations = allLocations.concat(locationData.filter(a => { if (a.country.indexOf(c) >= 0) return a }));
                        })
                    }
                    else {
                        allLocations = locationData;
                    }
                    allLocations.forEach((ele, index) => {
                        if (index == 0) {
                            var temp1 = ele.total_warehouse == 1 ? "is" : "are";
                            var temp2 = ele.total_warehouse == 1 ? "warehouse" : "warehouses";
                            res += `There ${temp1} ${ele.total_warehouse} ${temp2} in ${ele.country}`;
                        }
                        else if (index == (locationData.length - 1)) {
                            res += ` and ${ele.total_warehouse} in ${ele.country}`
                        }
                        else {
                            res += `,  ${ele.total_warehouse} in ${ele.country}`
                        }
                    })
                    botResponse.textResponse.push(res);
                    botResponse.voiceResponse = botResponse.textResponse;
                    botResponse.suggestion = {
                        "message": "",
                        "values": ["P1 Ticket Status", "Monthly Ticket Count", "Source Distribution", "Resolution Distribution", "Category Distribution"]
                    }
                    resolve(botResponse);
                }).catch(err => {
                    reject(err);
                })
        }
    })
    var final = await result;
    return final;
}

intentHandlers.warehouseTicketsCount = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        if (data["result"]["parameters"]["geo-state"].length) {
            var res = "";
            dataAPI.generalAPICall({ "cmd_id": "get_warehouse" }, token)
                .then(loc => {
                    var total_tickets = 0;
                    data["result"]["parameters"]["geo-state"].forEach(x => {

                        var arr = [];
                        var abb = require("../utils/states").getAbrrevaition(x);
                        Object.keys(loc).forEach(l => {
                            arr = arr.concat(loc[l].locations);
                        })
                        var allwarehouses = arr.filter(l => {
                            if ((l.location_id.indexOf(abb + ",") >= 0) && (typeof l.ticket_numbers !== "undefined")) { return l; }
                        });
                        var ticketsCount = []
                        allwarehouses.forEach(d => {
                            ticketsCount = ticketsCount.concat(d.ticket_numbers);
                        })
                        total_tickets = total_tickets + ticketsCount.length;
                    });
                    var wdata = total_tickets == 1 ? "There is one ticket" : `There are ${total_tickets} tickets`
                    var finalData = total_tickets == 0 ? "There are no tickets" : wdata
                    res = `${finalData} in ${data["result"]["parameters"]["geo-state"].join(", ")}`
                    botResponse.textResponse.push(res);
                    botResponse.voiceResponse = botResponse.textResponse;
                    botResponse.suggestion = {
                        "message": "",
                        "values": ["P1 Ticket Status", "Monthly Ticket Count", "Source Distribution", "Resolution Distribution", "Category Distribution"]
                    }
                    resolve(botResponse);
                })
        }
        else {
            dataAPI.generalAPICall({ "cmd_id": "get_warehouse_summary" }, token)
                .then(locationData => {
                    var res = "", allLocations = [];
                    if (data["result"]["parameters"]["geo-country"].length > 0) {
                        data["result"]["parameters"]["geo-country"].forEach((c, i) => {
                            allLocations = allLocations.concat(locationData.filter(a => { if (a.country.indexOf(c) >= 0) return a }));
                        })
                    }
                    else {
                        allLocations = locationData;
                    }
                    var emptyTickets = [];
                    allLocations.forEach((ele, index) => {

                        if (!ele.p1) {
                            emptyTickets.push(ele.country)
                        }
                        else if (index == 0 || res == "") {
                            var temp1 = ele.p1 == 1 ? "is" : "are";
                            var temp2 = ele.p1 == 1 ? "ticket" : "tickets";

                            res += `There ${temp1} ${ele.p1} ${temp2} in ${ele.country}`;
                        }
                        else {
                            res += `,  ${ele.p1} in ${ele.country}`
                        }
                    })
                    if (emptyTickets) {
                        if (res)
                            res += ` and  no ticket found in ${emptyTickets.join(", ")}.`
                        else {
                            res += `No ticket found in ${emptyTickets.join(", ")}.`
                        }
                    }
                    botResponse.textResponse.push(res);
                    botResponse.voiceResponse = botResponse.textResponse;
                    botResponse.suggestion = {
                        "message": "",
                        "values": ["P1 Ticket Status", "Monthly Ticket Count", "Source Distribution", "Resolution Distribution", "Category Distribution"]
                    }
                    resolve(botResponse);
                }).catch(err => {
                    reject(err);
                })
        }
    })
    var final = await result;
    return final;
}

intentHandlers.warehouseTicketsDifference = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        dataAPI.generalAPICall({ "cmd_id": "get_warehouse_summary" }, token)
            .then(locationData => {
                var sum = 0, finalRes = "";
                locationData.forEach((ele, index) => {
                    sum += ele.p1
                })
                if (sum) {
                    locationData.forEach((ele, index) => {
                        if (index == 0) {
                            finalRes += `${ele.country} has ${((ele.p1 / sum) * 100)} percent of total tickets`
                        }
                        else if (index == 1) {
                            finalRes += ` where as ${ele.country} has ${((ele.p1 / sum) * 100)} percent`
                        }
                        else if (index == (locationData.length - 1)) {
                            finalRes += ` and ${ele.country} has ${((ele.p1 / sum) * 100)} percent`
                        }
                        else {
                            finalRes += ` ${ele.country} has ${((ele.p1 / sum) * 100)} percent`
                        }
                    })
                    botResponse.textResponse.push(finalRes);
                    botResponse.voiceResponse.push(finalRes);
                }
                else {
                    botResponse.textResponse.push("No tickets found");
                    botResponse.voiceResponse.push("No tickets found");
                }
                botResponse.suggestion = {
                    "message": "",
                    "values": ["P1 Ticket Status", "Monthly Ticket Count", "Source Distribution", "Resolution Distribution", "Category Distribution"]
                }
                resolve(botResponse);
            }).catch(err => {
                reject(err);
            })
    })
    var final = await result;
    return final;
}

intentHandlers.warehouseResolvedtickets = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        dataAPI.generalAPICall({ "cmd_id": "get_warehouse" }, token)
            .then(locationData => {
                var allLocations = {};
                if (data["result"]["parameters"]["geo-country"].length > 0) {
                    data["result"]["parameters"]["geo-country"].forEach((c, i) => {
                        Object.keys(locationData).forEach(val => {
                            if (val.indexOf(c) >= 0)
                                allLocations[val] = locationData[val];
                        });
                    })
                }
                else {
                    allLocations = locationData;
                }

                Object.keys(allLocations).forEach((val, i) => {
                    if (i == 0) var respText = "Ticket status statistics goes as below : <br>\n"
                    // In Country out of N warehouses M are healthy, O are in progress, P are resolved recently, Q are  resolved recently
                    var respText = `${val} ticket Status :<br>\n Healthy : ${allLocations[val].summary.inprogress == 0 ? "None" : allLocations[val].summary.healthy}<br>\n`;
                    respText += ` In progress : ${allLocations[val].summary.inprogress == 0 ? "None" : allLocations[val].summary.inprogress}<br>\n`;
                    respText += ` Initiated recently : ${allLocations[val].summary.initial == 0 ? "None" : allLocations[val].summary.initial}<br>\n`;
                    respText += ` Resolved recently : ${allLocations[val].summary.recent_resolve == 0 ? "None" : allLocations[val].summary.recent_resolve}<br>\n`;


                    var resp = `In ${val}, out of ${allLocations[val].summary.total_warehouse} warehouse ${allLocations[val].summary.total_warehouse == 1 ? "ticket" : "tickets"}, ${allLocations[val].summary.healthy} ${allLocations[val].summary.healthy == 1 ? "is" : "are"} healthy,`;
                    resp += ` ${allLocations[val].summary.inprogress == 0 ? "No tickets in progress," : (allLocations[val].summary.inprogress + " " + (allLocations[val].summary.inprogress == 1 ? "is in progress," : "are in progress,"))}`;
                    resp += ` ${allLocations[val].summary.initial == 0 ? "No tickets initiated recently," : (allLocations[val].summary.initial + " " + (allLocations[val].summary.initial == 1 ? "was initiated recently," : "were initiated recently,"))}`;
                    resp += ` ${allLocations[val].summary.recent_resolve == 0 ? "No tickets resolved recently," : (allLocations[val].summary.recent_resolve + " " + (allLocations[val].summary.recent_resolve == 1 ? "was resolved recently," : "were resolved recently"))}`;
                    botResponse.textResponse.push(respText);
                    botResponse.voiceResponse.push(resp);
                });
                botResponse.suggestion = {}
                resolve(botResponse);
            }).catch(err => {
                reject(err);
            });
    })
    var final = await result;
    return final;
}

intentHandlers.dashboardSummary = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        dataAPI.generalAPICall({ "cmd_id": "get_cloud_expense" }, token)
            .then(cloudData => {
                var cloudServicesSummary = `Moving to cloud services expenses, ${cloudData[0].cloud_service.toUpperCase()} expenses ${cloudData[0].delta.indexOf("-") >= 0 ? "decreased" : "increased"} by ${cloudData[0].delta.replace(/[-]+/g, "")}, whereas  ${cloudData[1].cloud_service.toUpperCase()} expenses has ${cloudData[1].delta.indexOf("-") >= 0 ? "decreased" : "increased"} by ${cloudData[[1]].delta.replace(/[-]+/g, "")}`
                dataAPI.generalAPICall({ "cmd_id": "get_warehouse_summary" }, token)
                    .then(locationData => {
                        var res = "";
                        var emptyTickets = [];
                        locationData.forEach((ele, index) => {
                            var temp1 = ele.p1 == 1 ? "is" : "are";
                            var temp2 = ele.p1 == 1 ? "ticket" : "tickets";
                            var temp3 = ele.total_warehouse == 1 ? "warehouse" : "warehouses";

                            if (!ele.p1) {
                                emptyTickets.push(ele.country)
                            }
                            else if (index == 0 || res == "") {
                                res += `As of today, out of ${ele.total_warehouse} ${temp3} there ${temp1} ${ele.p1} P1 ${temp2} raised from warehouses in ${ele.country}`;
                            }
                            else {
                                res += `,  ${ele.p1} P1 ${temp2} out of ${ele.total_warehouse} ${temp3} from ${ele.country}`
                            }
                        })
                        if (emptyTickets) {
                            if (res)
                                res += ` and  no ticket found in ${emptyTickets.join(", ")}.`
                            else {
                                var x = `and ${emptyTickets[(emptyTickets.length - 1)]}`
                                emptyTickets[(emptyTickets.length - 1)] = x;
                                res += `As of today, no ticket found in ${emptyTickets.join(", ")}.`
                            }
                        }
                        botResponse.textResponse.push(res);
                        var voiceRes = res;
                        var found = config.privilegedUserGroup.find(function (element) {
                            return element.toLowerCase() == body.userRole.toLowerCase();
                        });
                        if (found) {
                            botResponse.textResponse.push(cloudServicesSummary);
                            voiceRes = voiceRes + ` While ${cloudData[0].cloud_service.toUpperCase()} expenses ${cloudData[0].delta.indexOf("-") >= 0 ? "decreased" : "increased"} by ${cloudData[0].delta.replace(/[$-]+/g, "")} dollars, whereas ${cloudData[1].cloud_service.toUpperCase()} expenses has ${cloudData[1].delta.indexOf("-") >= 0 ? "decreased" : "increased"} by ${cloudData[1].delta.replace(/[$-]+/g, "")} dollars`;
                        }
                        botResponse.voiceResponse.push(voiceRes);
                        botResponse.suggestion = {
                            "message": "",
                            "values": ["Monthly Ticket Count", "Source Distribution", "Resolution Distribution", "Category Distribution"]
                        }
                        resolve(botResponse);
                    }).catch(err => {
                        reject(err);
                    });
            }).catch(err => {
                reject(err);
            })
    })
    var final = await result;
    return final;
}

intentHandlers.cloudServicesTotalCost = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        var services = []
        if (data["result"]["parameters"]["cloud-services"].length > 0) {
            data["result"]["parameters"]["cloud-services"].forEach(servs => {
                var service = config.cloudServices.find(function (element) {
                    return element == servs;
                });
                if (typeof service != "undefined") services.push(servs);
            })
        }
        else {
            services = config.cloudServices;
        }

        botResponse.suggestion = {};
        var type = "total_cost";
        dataAPI.getCloudServicesSummary(services, type, token)
            .then(data => {
                if (data.length) {
                    if (data.length == 1) {
                        if (data[0].data) {
                            botResponse.textResponse.push(`Total ${data[0].type.toUpperCase()} cost stands at $ ${data[0].data.value}`);
                        }
                        else {
                            botResponse.textResponse.push(`Not able to fetch ${data[0].type.toUpperCase()} cost`);
                        }
                        botResponse.voiceResponse.push(`Total ${data[0].type.toUpperCase()} cost stands at ${data[0].data.value} dollars`);
                    }
                    else {
                        if (data[0].data) {
                            botResponse.textResponse.push(`While the total ${data[0].type.toUpperCase()} cost is $ ${data[0].data.value},`);
                        }
                        else {
                            botResponse.textResponse.push(`Not able to fetch ${data[0].type.toUpperCase()} cost`);
                        }
                        if (data[1].data) {
                            botResponse.textResponse.push(`${data[1].type.toUpperCase()} cost stands at $ ${data[1].data.value}`);
                        }
                        else {
                            botResponse.textResponse.push(`Not able to fetch ${data[1].type.toUpperCase()} cost`);
                        }
                        botResponse.voiceResponse.push(`The total costs for ${data[0].type.toUpperCase()} and ${data[1].type.toUpperCase()} stand at ${data[0].data.value} dollars and ${data[1].data.value} dollars respectively.`);
                    }
                }
                else {
                    botResponse.textResponse.push(`Not able to fetch cost data`);
                    botResponse.voiceResponse.push(`Not able to fetch cost data`);
                }
                botResponse.suggestion = {
                    "message": "",
                    "values": ["Monthly Ticket Count", "Source Distribution", "Resolution Distribution", "Category Distribution"]
                }
                resolve(botResponse);
            }).catch(err => {
                reject(err);
            })
    })
    var final = await result;
    return final;
}

intentHandlers.cloudServicesInstancesCount = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        var services = []
        if (data["result"]["parameters"]["cloud-services"].length > 0) {
            data["result"]["parameters"]["cloud-services"].forEach(servs => {
                var service = config.cloudServices.find(function (element) {
                    return element == servs;
                });
                if (typeof service != "undefined") services.push(servs);
            })
        }
        else {
            services = config.cloudServices;
        }
        botResponse.suggestion = {};
        var type = "total_running_instances";
        dataAPI.getCloudServicesSummary(services, type, token)
            .then(data => {
                if (data.length) {
                    if (data.length == 1) {
                        if (data[0].data) {
                            botResponse.textResponse.push(`Currently, there are ${data[0].data.value} running ${data[0].type.toUpperCase()} instances`);
                        }
                        else {
                            botResponse.textResponse.push(`Not able to fetch ${data[0].type.toUpperCase()} instances`);
                        }
                        botResponse.voiceResponse = botResponse.textResponse;
                    }
                    else {
                        if (data[0].data) {
                            botResponse.textResponse.push(`Currently, there are ${data[0].data.value} running ${data[0].type.toUpperCase()} instances`);
                        }
                        else {
                            botResponse.textResponse.push(`Not able to fetch ${data[0].type.toUpperCase()} instances`);
                        }
                        if (data[1].data) {
                            botResponse.textResponse.push(`There are ${data[1].data.value} ${data[1].type.toUpperCase()} instances running`);
                        }
                        else {
                            botResponse.textResponse.push(`Not able to fetch ${data[1].type.toUpperCase()} instances`);
                        }
                        botResponse.voiceResponse.push(`The total running instances in ${data[0].type.toUpperCase()} and ${data[1].type.toUpperCase()} are ${data[0].data.value} and ${data[1].data.value} respectively.`);
                    }
                }
                else {
                    botResponse.textResponse.push(`Not able to fetch cloud instances data`);
                    botResponse.voiceResponse.push(`Not able to fetch cloud instances data`);
                }
                botResponse.suggestion = {
                    "message": "",
                    "values": ["Monthly Ticket Count", "Source Distribution", "Resolution Distribution", "Category Distribution"]
                }
                resolve(botResponse);
            }).catch(err => {
                reject(err);
            })
    })
    var final = await result;
    return final;
}

intentHandlers.cloudServicesTopSpent = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        var services = [], topSpent = ["total_spent_account"], topSpent2 = "account";
        if (data["result"]["parameters"]["cloud-services"].length > 0) {
            data["result"]["parameters"]["cloud-services"].forEach(servs => {
                var service = config.cloudServices.find(function (element) {
                    return element == servs;
                });
                if (typeof service != "undefined") services.push(servs);
            })
        }
        else {
            services = config.cloudServices;
        }
        if (data["result"]["parameters"]["top-spent"]) {
            if (data["result"]["parameters"]["top-spent"] == "service") {
                topSpent = ["total_spent_service"];
                topSpent2 = "service";
            }
            else if (data["result"]["parameters"]["top-spent"] == "resource-location") {
                topSpent = ["top_spent_resource_location"];
                topSpent2 = "resource location";
            }
        }
        botResponse.suggestion = {};
        dataAPI.getcloudServicesTopSpent(services, topSpent, token)
            .then(data => {
                if (data.length) {
                    if (data.length == 1) {
                        if (data[0].data) {
                            botResponse.textResponse.push(`Top spent ${topSpent2} in ${data[0].serviceType.toUpperCase()} is ${data[0].data.name} with a spend of $ ${data[0].data.value}`);
                        }
                        else {
                            botResponse.textResponse.push(`Not able to fetch Top spent ${topSpent2} in ${data[0].serviceType.toUpperCase()}`);
                        }
                        botResponse.voiceResponse.push(`Top spent ${topSpent2} in ${data[0].serviceType.toUpperCase()} is ${data[0].data.name} with a spend of ${data[0].data.value} dollars`);
                    }
                    else {
                        if (data[0].data) {
                            botResponse.textResponse.push(`Top spent ${topSpent2} in ${data[0].serviceType.toUpperCase()} is ${data[0].data.name} with a spend of $ ${data[0].data.value}`);
                        }
                        else {
                            botResponse.textResponse.push(`Not able to fetch Top spent ${topSpent2} in ${data[0].serviceType.toUpperCase()}`);
                        }
                        if (data[1].data) {
                            botResponse.textResponse.push(`Top spent ${topSpent2} in ${data[1].serviceType.toUpperCase()} is ${data[1].data.name} with a spend of $ ${data[1].data.value}`);
                        }
                        else {
                            botResponse.textResponse.push(`Not able to fetch Top spent ${topSpent2} in ${data[1].serviceType.toUpperCase()}`);
                        }
                        botResponse.voiceResponse.push(`Top spent ${topSpent2}s in ${data[0].serviceType.toUpperCase()} and ${data[1].serviceType.toUpperCase()} are ${data[0].data.name} with a spend of ${data[0].data.value} dollars and ${data[1].data.name} with a spend of ${data[1].data.value} dollars respectively.`);
                    }
                }
                else {
                    botResponse.textResponse.push(`Not able to fetch to spent items`);
                    botResponse.voiceResponse.push(`Not able to fetch to spent items`);
                    botResponse.suggestion = {
                        "message": "",
                        "values": ["Monthly Ticket Count", "Source Distribution", "Resolution Distribution", "Category Distribution"]
                    }
                }
                resolve(botResponse);
            }).catch(err => {
                reject(err);
            })
    })
    var final = await result;
    return final;
}

intentHandlers.warehouseMonthlyTickets = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        var allTickets = "", voiceTickets = "";
        dataAPI.generalAPICall({ "cmd_id": "get_historical_data", "chart_type": "incidence_count_by_month" }, token)
            .then(apiData => {
                if (apiData.length > 0) {
                    botResponse.textResponse.push(`Below are the monthly tickets :<br>\n`);
                    apiData[0].values.forEach((data, index) => {
                        var t = data.y == 1 ? "ticket" : "tickets";
                        var c = data.y == 1 ? "is" : "were"
                        if (index == 0) {
                            voiceTickets += `There ${c} ${data.y} ${t} in ${utils.getMonth(parseInt(data.x.split("-")[1]))}`
                        }
                        else if (index == (apiData[0].values.length - 1)) {
                            voiceTickets += ` and ${data.y} in the month of ${utils.getMonth(parseInt(data.x.split("-")[1]))}`
                        }
                        else {
                            voiceTickets += `, ${data.y} ${t} in ${utils.getMonth(parseInt(data.x.split("-")[1]))}`
                        }
                        allTickets += `${data.y} in ${utils.getMonth(parseInt(data.x.split("-")[1]))} ${data.x.split("-")[0]} <br>\n`
                    });
                    botResponse.textResponse.push(allTickets);
                    botResponse.voiceResponse.push(voiceTickets);
                }
                else {
                    botResponse.textResponse.push(`Monthly tickets not found`);
                    botResponse.voiceResponse.push(`Monthly tickets not found`);
                }
                botResponse.suggestion = {
                    "message": "",
                    "values": ["Source Distribution", "Resolution Distribution", "Category Distribution"]
                }
                resolve(botResponse);
            }).catch(err => {
                reject(err);
            });
    })
    var final = await result;
    return final;
}

intentHandlers.warehouseMonthofHighestTickets = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        dataAPI.generalAPICall({ "cmd_id": "get_historical_data", "chart_type": "incidence_count_by_month" }, token)
            .then(apiData => {
                if (apiData.length > 0) {

                    if ((data["result"]["parameters"]["monthlyData"] == "min")) {
                        var objMin = apiData[0].values.filter((a) => { if (a.y == Math.min(...apiData[0].values.map((a) => a.y))) return a })[0]
                        // var final = `${utils.getMonth(parseInt(objMin.x.split("-")[1]))} ${objMin.x.split("-")[0]} has ${objMin.y} tickets`
                        var final = `With ${objMin.y} tickets, ${utils.getMonth(parseInt(objMin.x.split("-")[1]))} ${objMin.x.split("-")[0]} has lowest number of tickets`
                    }
                    else {
                        var objMax = apiData[0].values.filter((a) => { if (a.y == Math.max(...apiData[0].values.map((a) => a.y))) return a })[0]
                        // var final = `${utils.getMonth(parseInt(objMax.x.split("-")[1]))} ${objMax.x.split("-")[0]} has ${objMax.y} tickets`
                        var final = `With ${objMax.y} tickets, ${utils.getMonth(parseInt(objMax.x.split("-")[1]))} ${objMax.x.split("-")[0]} has highest number of tickets`
                    }

                    botResponse.textResponse.push(final);
                    botResponse.voiceResponse.push(final);
                }
                else {
                    botResponse.textResponse.push(`Tickets not found`);
                    botResponse.voiceResponse = botResponse.textResponse;
                }
                botResponse.suggestion = {
                    "message": "",
                    "values": ["Source Distribution", "Resolution Distribution", "Category Distribution"]
                }
                resolve(botResponse);
            }).catch(err => {
                reject(err);
            });
    })
    var final = await result;
    return final;
}

intentHandlers.warehouseContributingFactor = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        dataAPI.generalAPICall({ "cmd_id": "get_historical_data", "chart_type": "incidence_count_by_assignment" }, token)
            .then(apiData => {
                if (apiData.length > 0) {
                    // This line filters month which has hiehest number of tickets 
                    var obj = apiData[0].values.filter((a) => { if (a.y == Math.max(...apiData[0].values.map((a) => a.y))) return a })[0]
                    var final = `${obj.x} with ${obj.y} tickets is the main contributing factor`
                    var allOther = ""
                    apiData[0].values.forEach((a, i) => {
                        var ticket = a.y == 1 ? "ticket" : "tickets";
                        if (i == 0) {
                            allOther += `We have ${a.y} ${ticket} from ${a.x}`;
                        }
                        else if (i == (apiData[0].values.length - 1)) {
                            allOther += ` and ${a.y} from ${a.x}`;
                        }
                        else {
                            allOther += `, ${a.y} from ${a.x}`;
                        }
                    })
                    botResponse.textResponse.push(final);
                    botResponse.textResponse.push(allOther);
                    botResponse.voiceResponse.push(final);
                }
                else {
                    botResponse.textResponse.push(`No data found for contributing factor`);
                    botResponse.voiceResponse = botResponse.textResponse;
                }
                botResponse.suggestion = {
                    "message": "",
                    "values": ["Source Distribution", "Resolution Distribution", "Category Distribution"]
                }
                resolve(botResponse);
            }).catch(err => {
                reject(err);
            });
    })
    var final = await result;
    return final;
}

intentHandlers.chatbotWarehouseReport = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        dataAPI.generalAPICall({ "cmd_id": "get_warehouse_summary" }, token)
            .then(locationData => {
                var res = "";
                var emptyTickets = [];
                locationData.forEach((ele, index) => {
                    var temp1 = ele.p1 == 1 ? "is" : "are";
                    var temp2 = ele.p1 == 1 ? "ticket" : "tickets";
                    var temp3 = ele.total_warehouse == 1 ? "warehouse" : "warehouses";

                    if (!ele.p1) {
                        emptyTickets.push(ele.country)
                    }
                    else if (index == 0 || res == "") {
                        res += `As of today, out of ${ele.total_warehouse} ${temp3} there ${temp1} ${ele.p1} P1 ${temp2} raised from warehouses in ${ele.country}`;
                    }
                    else {
                        res += `,  ${ele.p1} P1 ${temp2} out of ${ele.total_warehouse} ${temp3} from ${ele.country}`
                    }
                })
                if (emptyTickets) {
                    if (res)
                        res += ` and  no ticket found in ${emptyTickets.join(", ")}.`
                    else {
                        var x = `and ${emptyTickets[(emptyTickets.length - 1)]}`
                        emptyTickets[(emptyTickets.length - 1)] = x;
                        res += `As of today, no ticket found in ${emptyTickets.join(", ")}.`
                    }
                }
                botResponse.textResponse.push(res);
                botResponse.voiceResponse = botResponse.textResponse;
                botResponse.suggestion = {};
                resolve(botResponse);
            }).catch(err => {
                reject(err);
            })
    })
    var final = await result;
    return final;
}

intentHandlers.chatbotCloudServicesExpenseReport = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        var services = []
        if (data["result"]["parameters"]["cloud-services"].length > 0) {
            data["result"]["parameters"]["cloud-services"].forEach(servs => {
                var service = config.cloudServices.find(function (element) {
                    return element == servs;
                });
                if (typeof service != "undefined") {
                    services.push(servs);
                }
            })
        }
        else {
            services = config.cloudServices;
        }
        botResponse.suggestion = {};
        var topSpentCMD = ["total_spent_service", "total_spent_account", "top_spent_resource_location"];
        var topSpentValues = {
            "total_spent_service": "service",
            "total_spent_account": "account",
            "top_spent_resource_location": "resource location"
        }
        var topSpent = [], instances = [], cost = [];
        var topSpentVoice = [], instancesVoice = [], costVoice = [];

        dataAPI.getcloudServicesTopSpent(services, topSpentCMD, token)
            .then(topSpentData => {
                if (topSpentData.length) {
                    topSpentData.forEach(topSpentItems => {
                        topSpent.push(`Top spent ${topSpentValues[topSpentItems.dataType]} : ${topSpentItems.data.name} with top spent of $${topSpentItems.data.value}<br>\n`);
                        topSpentVoice.push(`top spent ${topSpentValues[topSpentItems.dataType]} is ${topSpentItems.data.name} with top spent of ${topSpentItems.data.value} dollars `);
                    })
                }
                else {
                    botResponse.textResponse.push(`Not able to fetch to spent items`);
                }
                var type = "total_running_instances";
                dataAPI.getCloudServicesSummary(services, type, token)
                    .then(instancesCountData => {
                        if (instancesCountData.length) {
                            instancesCountData.forEach((instancesData, i) => {
                                instances.push(`Instances : ${instancesData.data.value}, `);
                                instancesVoice.push(`with ${instancesData.data.value} instances running.`);
                            });
                        }
                        else {
                            botResponse.textResponse.push(`Not able to fetch cloud instances data`);
                        }
                        var type = "total_cost";
                        dataAPI.getCloudServicesSummary(services, type, token)
                            .then(totalCostData => {
                                if (totalCostData.length) {
                                    totalCostData.forEach((totalCost, i) => {
                                        cost.push(`Cloud expense for ${totalCost.type.toUpperCase()} goes as follows :<br>\n Cost : $ ${totalCost.data.value}, `);
                                        costVoice.push(`Total ${totalCost.type.toUpperCase()} cost is ${totalCost.data.value} dollars`);
                                    });
                                }
                                else {
                                    botResponse.textResponse.push(`Not able to fetch cost data`);
                                }

                                if (cost.length || instances.length || topSpent.length) {
                                    cost.forEach((d, i) => {
                                        botResponse.textResponse.push(`${cost[i]} <br>\n ${instances[i]} <br>\n ${topSpent[i]} ${topSpent[i + (cost.length)]} ${topSpent[i + (2 * (cost.length))]}<br>\n`);
                                        botResponse.voiceResponse.push(`${costVoice[i]} ${instancesVoice[i]}`);
                                    })
                                }

                                // if (costVoice.length || instancesVoice.length || topSpentVoice.length) {
                                //     // botResponse.voiceResponse.push(`Cloud services report is given below`)
                                //     costVoice.forEach((d, i) => {
                                //         botResponse.voiceResponse.push(`${costVoice[i]} ${instancesVoice[i]}`)                                       
                                //     })
                                // }
                                resolve(botResponse);
                            }).catch(err => {
                                reject(err);
                            })
                    }).catch(err => {
                        reject(err);
                    });
            }).catch(err => {
                reject(err);
            })
    })
    var final = await result;
    return final;
}

intentHandlers.chatbotWarehouseSourceDistributionStatistics = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        dataAPI.generalAPICall({ "cmd_id": "get_historical_data", "chart_type": "source_distribution" }, token)
            .then(apiData => {
                if (apiData.length > 0) {
                    var sd = "Source distribution data is given below : <br>\n";
                    var sdVoice = "Source distribution goes as follows.";
                    apiData.forEach((sdData, index) => {
                        sd += `${sdData.x} - ${sdData.y}<br>\n`
                        if (index == (apiData.length - 1)) {
                            sdVoice += `and ${sdData.y} tickets are through ${sdData.x}.`
                        }
                        else if (index <= 2) {
                            sdVoice += `${sdData.y} tickets are through ${sdData.x}, `
                        }
                    });
                    botResponse.textResponse.push(sd);
                    botResponse.voiceResponse.push(sdVoice + ` More details are shown to you on the screen.`);
                }
                else {
                    botResponse.textResponse.push(`No source distribution data found`);
                    botResponse.voiceResponse.push(`No source distribution data found`);
                }

                resolve(botResponse);
            }).catch(err => {
                reject(err);
            });
    })
    var final = await result;
    return final;
}

intentHandlers.chatbotWarehouseResolutionDistributionStatistics = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        dataAPI.generalAPICall({ "cmd_id": "get_historical_data", "chart_type": "resolution_distribution" }, token)
            .then(apiData => {
                if (apiData.length > 0) {
                    var sd = "Resolution distribution data is given below : <br>\n";
                    var sdVoice = "Resolution distribution goes as follows.";
                    apiData.forEach((sdData, index) => {
                        sd += `${sdData.x} - ${sdData.y}%<br>\n`;
                        if (index <= 2) {
                            sdVoice += `${sdData.y}% were ${sdData.x},`
                        }
                    });
                    botResponse.textResponse.push(sd);
                    botResponse.voiceResponse.push(sdVoice + ` More details are shown to you on the screen.`);
                }
                else {
                    botResponse.textResponse.push(`No resolution distribution data found`);
                    botResponse.voiceResponse.push(`No resolution distribution data found`);
                }

                resolve(botResponse);
            }).catch(err => {
                reject(err);
            });
    })
    var final = await result;
    return final;
}

intentHandlers.chatbotWarehouseCategoryDistributionStatistics = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        dataAPI.generalAPICall({ "cmd_id": "get_historical_data", "chart_type": "category_distribution" }, token)
            .then(apiData => {
                if (apiData.length > 0) {
                    var sd = "Alright! Category distribution data is given below : <br>\n";
                    var sdVoice = "Category distribution data is given below.";
                    apiData[0].values.forEach((sdData, index) => {
                        sd += `${sdData.x} - ${sdData.y}%<br>\n`
                        if (index == (apiData[0].values.length - 1)) {
                            sdVoice += ` and ${sdData.x.replace(/-/g, " ")} belongs to ${sdData.y} percent.`
                        }
                        else if (index <= 2) {
                            sdVoice += `${sdData.x.replace(/-/g, " ")} belongs to ${sdData.y} percent,`
                        }
                    });
                    botResponse.textResponse.push(sd);
                    botResponse.voiceResponse.push(sdVoice + ` More details are shown to you on the screen.`);
                }
                else {
                    botResponse.textResponse.push(`No category distribution data found`);
                    botResponse.voiceResponse.push(`No category distribution data found`);
                }
                resolve(botResponse);
            }).catch(err => {
                reject(err);
            });
    })
    var final = await result;
    return final;
}

intentHandlers.cloudServicesCostByServices = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        var services = []
        if (data["result"]["parameters"]["cloud-services"].length > 0) {
            data["result"]["parameters"]["cloud-services"].forEach(servs => {
                var service = config.cloudServices.find(function (element) {
                    return element == servs;
                });
                if (typeof service != "undefined") services.push(servs);
            })
        }
        else {
            services = config.cloudServices;
        }
        if (services.length == 1) {
            dataAPI.generalAPICall({ "cmd_id": "get_cost_details", "cloud_service": services[0], "chart_type": "cost_by_service" }, token)
                .then(apiData => {
                    if (apiData.length > 0) {
                        var sd = `Cloud services cost by services data for ${services[0].toUpperCase()} is given below :<br>\n`;
                        var sdVoice = `Cloud services cost by services data for ${services[0].toUpperCase()} is given below.`;
                        apiData[0].values.forEach((sdData, index) => {
                            sd += `${sdData.x} - $${sdData.y}<br>\n`
                            if (index == (apiData[0].values.length - 1)) {
                                sdVoice += ` and ${sdData.y} dollars for ${sdData.x}.`
                            }
                            else if (index <= 2) {
                                sdVoice += `${sdData.y} dollars for ${sdData.x},`
                            }

                        });
                        botResponse.textResponse.push(sd);
                        botResponse.voiceResponse.push(sdVoice + ` More details are shown to you on the screen.`);
                    }
                    else {
                        botResponse.textResponse.push(`Data not found`);
                        botResponse.voiceResponse.push(`Data not found`);
                    }

                    resolve(botResponse);
                }).catch(err => {
                    reject(err);
                });
        }
        else {
            dataAPI.generalAPICallForCost(services, "cost_by_service", token)
                .then(apiData => {
                    if (apiData.length > 0) {
                        var sdVoice = `Cloud services cost by services data for ${services.join(", ").toUpperCase()} are show on the screen.`;
                        var sd = "";
                        apiData.forEach((allData, i) => {
                            sd += `Cloud services cost by services data for ${services[i].toUpperCase()} is given below :<br>\n`;
                            allData.values.forEach((sdData, index) => {
                                sd += `${sdData.x} - $${sdData.y}<br>\n`
                            });
                            sd += `<br>\n`
                        })
                        botResponse.textResponse.push(sd);
                        botResponse.voiceResponse.push(sdVoice);
                    }
                    else {
                        botResponse.textResponse.push(`Data not found`);
                        botResponse.voiceResponse.push(`Data not found`);
                    }

                    resolve(botResponse);
                }).catch(err => {
                    reject(err);
                });
        }
    })
    var final = await result;
    return final;
}

intentHandlers.cloudServicesCostByAccounts = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        var services = []
        if (data["result"]["parameters"]["cloud-services"].length > 0) {
            data["result"]["parameters"]["cloud-services"].forEach(servs => {
                var service = config.cloudServices.find(function (element) {
                    return element == servs;
                });
                if (typeof service != "undefined") services.push(servs);
            })
        }
        else {
            services = config.cloudServices;
        }
        if (services.length == 1) {
            dataAPI.generalAPICall({ "cmd_id": "get_cost_details", "cloud_service": services[0], "chart_type": "cost_by_account" }, token)
                .then(apiData => {
                    if (apiData.length > 0) {
                        var sd = `Cloud services cost by accounts data for ${services[0].toUpperCase()}  is given below :<br>\n`;
                        var sdVoice = `Cloud services cost by accounts data for ${services[0].toUpperCase()} is given below.`;
                        apiData[0].values.forEach((sdData, index) => {
                            sd += `${sdData.x} - $${sdData.y}<br>\n`
                            if (index == (apiData[0].values.length - 1)) {
                                sdVoice += ` and ${sdData.y} dollars for ${sdData.x}.`
                            }
                            else if (index <= 2) {
                                sdVoice += `${sdData.y} dollars for ${sdData.x},`
                            }

                        });
                        botResponse.textResponse.push(sd);
                        botResponse.voiceResponse.push(sdVoice + ` More details are shown to you on the screen.`);
                    }
                    else {
                        botResponse.textResponse.push(`Data not found`);
                        botResponse.voiceResponse.push(`Data not found`);
                    }

                    resolve(botResponse);
                }).catch(err => {
                    reject(err);
                });
        }
        else {
            dataAPI.generalAPICallForCost(services, "cost_by_account", token)
                .then(apiData => {
                    if (apiData.length > 0) {
                        var sdVoice = `Cloud services cost by accounts data for ${services.join(", ").toUpperCase()} are show on the screen.`;
                        var sd = "";
                        apiData.forEach((allData, i) => {
                            sd += `Cloud services cost by accounts data for ${services[i].toUpperCase()} is given below :<br>\n`;
                            allData.values.forEach((sdData, index) => {
                                sd += `${sdData.x} - $${sdData.y}<br>\n`
                            });
                            sd += `<br>\n`
                        })
                        botResponse.textResponse.push(sd);
                        botResponse.voiceResponse.push(sdVoice);
                    }
                    else {
                        botResponse.textResponse.push(`Data not found`);
                        botResponse.voiceResponse.push(`Data not found`);
                    }

                    resolve(botResponse);
                }).catch(err => {
                    reject(err);
                });
        }
    })
    var final = await result;
    return final;
}

intentHandlers.cloudServicesCostByLocations = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        var services = []
        if (data["result"]["parameters"]["cloud-services"].length > 0) {
            data["result"]["parameters"]["cloud-services"].forEach(servs => {
                var service = config.cloudServices.find(function (element) {
                    return element == servs;
                });
                if (typeof service != "undefined") services.push(servs);
            })
        }
        else {
            services = config.cloudServices;
        }

        if (services.length == 1) {
            dataAPI.generalAPICall({ "cmd_id": "get_cost_details", "cloud_service": services[0], "chart_type": "cost_by_resource_location" }, token)
                .then(apiData => {
                    if (apiData.length > 0) {
                        var sd = `Cloud services cost by locations data for ${services[0].toUpperCase()} is given below :<br>\n`;
                        var sdVoice = `Cloud services cost by locations data for ${services[0].toUpperCase()} is given below.`;
                        apiData[0].values.forEach((sdData, index) => {
                            sd += `${sdData.x} - $${sdData.y}<br>\n`
                            if (index == (apiData[0].values.length - 1)) {
                                sdVoice += ` and ${sdData.y} dollars for ${sdData.x}.`
                            }
                            else if (index <= 2) {
                                sdVoice += `${sdData.y} dollars for ${sdData.x},`
                            }

                        });
                        botResponse.textResponse.push(sd);
                        botResponse.voiceResponse.push(sdVoice + ` More details are shown to you on the screen.`);
                    }
                    else {
                        botResponse.textResponse.push(`Data not found`);
                        botResponse.voiceResponse.push(`Data not found`);
                    }

                    resolve(botResponse);
                }).catch(err => {
                    reject(err);
                });
        } else {
            dataAPI.generalAPICallForCost(services, "cost_by_resource_location", token)
                .then(apiData => {
                    if (apiData.length > 0) {
                        var sdVoice = `Cloud services cost by locations data for ${services.join(", ").toUpperCase()} are show on the screen.`;
                        var sd = "";
                        apiData.forEach((allData, i) => {
                            sd += `Cloud services cost by locations data for ${services[i].toUpperCase()} is given below :<br>\n`;
                            allData.values.forEach((sdData, index) => {
                                sd += `${sdData.x} - $${sdData.y}<br>\n`
                            });
                            sd += `<br>\n`
                        })
                        botResponse.textResponse.push(sd);
                        botResponse.voiceResponse.push(sdVoice);
                    }
                    else {
                        botResponse.textResponse.push(`Data not found`);
                        botResponse.voiceResponse.push(`Data not found`);
                    }

                    resolve(botResponse);
                }).catch(err => {
                    reject(err);
                });
        }

    })
    var final = await result;
    return final;
}

intentHandlers.cloudServicesCostByEnvTag = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        var services = []
        if (data["result"]["parameters"]["cloud-services"].length > 0) {
            data["result"]["parameters"]["cloud-services"].forEach(servs => {
                var service = config.cloudServices.find(function (element) {
                    return element == servs;
                });
                if (typeof service != "undefined") services.push(servs);
            })
        }
        else {
            services = config.cloudServices;
        }


        if (services.length == 1) {
            dataAPI.generalAPICall({ "cmd_id": "get_cost_details", "cloud_service": services[0], "chart_type": "cost_by_env_tag" }, token)
                .then(apiData => {
                    if (apiData.length > 0) {
                        var sd = `Cloud services cost by environment data  for ${services[0].toUpperCase()} is given below :<br>\n`;
                        var sdVoice = `Cloud services cost by environment data for ${services[0].toUpperCase()} is given below.`;
                        apiData[0].values.forEach((sdData, index) => {
                            sd += `${sdData.x} - $${sdData.y}<br>\n`
                            if (index == (apiData[0].values.length - 1)) {
                                sdVoice += ` and ${sdData.y} dollars for ${sdData.x}.`
                            }
                            else if (index <= 2) {
                                sdVoice += `${sdData.y} dollars for ${sdData.x},`
                            }

                        });
                        botResponse.textResponse.push(sd);
                        botResponse.voiceResponse.push(sdVoice + ` More details are shown to you on the screen.`);
                    }
                    else {
                        botResponse.textResponse.push(`Data not found`);
                        botResponse.voiceResponse.push(`Data not found`);
                    }

                    resolve(botResponse);
                }).catch(err => {
                    reject(err);
                });
        } else {
            dataAPI.generalAPICallForCost(services, "cost_by_env_tag", token)
                .then(apiData => {
                    if (apiData.length > 0) {
                        var sdVoice = `Cloud services cost by environment data for ${services.join(", ").toUpperCase()} are show on the screen.`;
                        var sd = "";
                        apiData.forEach((allData, i) => {
                            sd += `Cloud services cost by environment data for ${services[i].toUpperCase()} is given below :<br>\n`;
                            allData.values.forEach((sdData, index) => {
                                sd += `${sdData.x} - $${sdData.y}<br>\n`
                            });
                            sd += `<br>\n`
                        })
                        botResponse.textResponse.push(sd);
                        botResponse.voiceResponse.push(sdVoice);
                    }
                    else {
                        botResponse.textResponse.push(`Data not found`);
                        botResponse.voiceResponse.push(`Data not found`);
                    }

                    resolve(botResponse);
                }).catch(err => {
                    reject(err);
                });
        }
    })
    var final = await result;
    return final;
}

intentHandlers.cloudServicesCostByResourceGroup = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        var services = []
        if (data["result"]["parameters"]["cloud-services"].length > 0) {
            data["result"]["parameters"]["cloud-services"].forEach(servs => {
                var service = config.cloudServices.find(function (element) {
                    return element == servs;
                });
                if (typeof service != "undefined") services.push(servs);
            })
        }
        else {
            services = config.cloudServices;
        }


        if (services.length == 1) {
            dataAPI.generalAPICall({ "cmd_id": "get_cost_details", "cloud_service": services[0], "chart_type": "cost_by_resource_group" }, token)
                .then(apiData => {
                    if (apiData.length > 0) {
                        var sd = `Cloud services cost by resource group data for ${services[0].toUpperCase()} is given below :<br>\n`;
                        var sdVoice = `Cloud services cost by resource group data for ${services[0].toUpperCase()} is given below.`;
                        apiData.forEach((sdData, index) => {
                            sd += `${sdData.x} - $${sdData.y}<br>\n`
                            if (index == (apiData.length - 1)) {
                                sdVoice += ` and ${sdData.y} dollars for ${sdData.x}.`
                            }
                            else if (index <= 2) {
                                sdVoice += `${sdData.y} dollars for ${sdData.x},`
                            }

                        });
                        botResponse.textResponse.push(sd);
                        botResponse.voiceResponse.push(sdVoice + ` More details are shown to you on the screen.`);
                    }
                    else {
                        botResponse.textResponse.push(`Data not found`);
                        botResponse.voiceResponse.push(`Data not found`);
                    }

                    resolve(botResponse);
                }).catch(err => {
                    reject(err);
                });
        } else {
            dataAPI.generalAPICallForCost(services, "cost_by_resource_group", token)
                .then(apiData => {
                    if (apiData.length > 0) {
                        var sdVoice = `Cloud services cost by resource group data for ${services.join(", ").toUpperCase()} are show on the screen.`;
                        var sd = "";
                        apiData.forEach((allData, i) => {
                            sd += `Cloud services cost by resource group data for ${services[i].toUpperCase()} is given below :<br>\n`;
                            allData.forEach((sdData, index) => {
                                sd += `${sdData.x} - $${sdData.y}<br>\n`
                            });
                            sd += `<br>\n`
                        })
                        botResponse.textResponse.push(sd);
                        botResponse.voiceResponse.push(sdVoice);
                    }
                    else {
                        botResponse.textResponse.push(`Data not found`);
                        botResponse.voiceResponse.push(`Data not found`);
                    }

                    resolve(botResponse);
                }).catch(err => {
                    reject(err);
                });
        }
    })
    var final = await result;
    return final;
}

intentHandlers.chatbotWarehouseManagerDetails = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        if (data["result"]["parameters"]["geo-city"].length <= 0) {
            botResponse.textResponse.push(data["result"]["fulfillment"]["speech"]);
            botResponse.voiceResponse.push(data["result"]["fulfillment"]["speech"]);
            resolve(botResponse);
        }
        else {
            var locationIDs = cities.getLocationID(data["result"]["parameters"]["geo-city"][0]);
            dataAPI.generalAPICall({ "cmd_id": "get_warehouse" }, token)
                .then(locationData => {
                    var locationData2 = [];
                    Object.keys(locationData).forEach(val => {
                        locationData2 = locationData2.concat(locationData[val].locations)
                    })
                    var alltickets = locationData2.filter(a => {
                        for (let i = 0; i < locationIDs.length; i++) {
                            if ((a.location_id) == locationIDs[i])
                                return a
                        }
                    })
                    var alltickets2 = alltickets.map(a => { if (a.ticket_numbers) return (a.ticket_numbers) }).filter(a => { if (a) return a });
                    var mergedAlltickets2 = [].concat.apply([], alltickets2);
                    if (mergedAlltickets2.length <= 0) {
                        botResponse.textResponse.push(`No details found`);
                        botResponse.voiceResponse.push(`No details found`);
                        resolve(botResponse);
                    }
                    else {
                        var ticket_numbers = [];
                        ticket_numbers.push(mergedAlltickets2[0]);
                        dataAPI.generalAPICall({ "cmd_id": "get_details", "ticket_numbers": ticket_numbers }, token)
                            .then(allTicketsData => {
                                if (allTicketsData[0]) {
                                    allTicketsData[0].forEach(a => {
                                        if (a.header == "Owner") {
                                            botResponse.textResponse.push(`${a.value.text.split("@")[0].split(".").join(" ")} is the manager`);
                                            botResponse.voiceResponse.push(`${a.value.text.split("@")[0].split(".").join(" ")} is the manager`);
                                            resolve(botResponse);
                                        }
                                    })
                                }
                                else {
                                    botResponse.textResponse.push(`No details found`);
                                    botResponse.voiceResponse.push(`No details found`);
                                    resolve(botResponse);
                                }
                            }).catch(err => {
                                reject(err);
                            });
                    }
                }).catch(err => {
                    reject(err);
                });
        }
    })
    var final = await result;
    return final;
}

intentHandlers.chatbotWarehouseBussinessImpact = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        if (data["result"]["parameters"]["geo-city"].length <= 0) {
            botResponse.textResponse.push(data["result"]["fulfillment"]["speech"]);
            botResponse.voiceResponse.push(data["result"]["fulfillment"]["speech"]);
            resolve(botResponse);
        }
        else {
            var locationIDs = cities.getLocationID(data["result"]["parameters"]["geo-city"][0]);
            dataAPI.generalAPICall({ "cmd_id": "get_warehouse" }, token)
                .then(locationData => {
                    var locationData2 = [];
                    Object.keys(locationData).forEach(val => {
                        locationData2 = locationData2.concat(locationData[val].locations)
                    })
                    var alltickets = locationData2.filter(a => {
                        for (let i = 0; i < locationIDs.length; i++) {
                            if ((a.location_id) == locationIDs[i])
                                return a
                        }
                    })
                    var alltickets2 = alltickets.map(a => { if (a.ticket_numbers) return (a.ticket_numbers) }).filter(a => { if (a) return a });
                    var mergedAlltickets2 = [].concat.apply([], alltickets2);
                    if (mergedAlltickets2.length <= 0) {
                        botResponse.textResponse.push(`No details found`);
                        botResponse.voiceResponse.push(`No details found`);
                        resolve(botResponse);
                    }
                    else {
                        var ticket_numbers = [];
                        ticket_numbers.push(mergedAlltickets2[0]);
                        dataAPI.generalAPICall({ "cmd_id": "get_details", "ticket_numbers": ticket_numbers }, token)
                            .then(allTicketsData => {
                                if (allTicketsData[0]) {
                                    allTicketsData[0].forEach(a => {
                                        if (a.header == "Business Impact") {
                                            botResponse.textResponse.push(a.value.text);
                                            botResponse.voiceResponse = botResponse.textResponse;
                                            resolve(botResponse);
                                        }
                                    })
                                }
                                else {
                                    botResponse.textResponse.push(`No details found`);
                                    botResponse.voiceResponse = botResponse.textResponse;
                                    resolve(botResponse);
                                }
                            }).catch(err => {
                                reject(err);
                            });
                    }
                }).catch(err => {
                    reject(err);
                });
        }
    })
    var final = await result;
    return final;
}

intentHandlers.chatbotWarehouseOutageWindow = async (data, body, botResponse, token) => {
    var result = new Promise((resolve, reject) => {
        if (data["result"]["parameters"]["geo-city"].length <= 0) {
            botResponse.textResponse.push(data["result"]["fulfillment"]["speech"]);
            botResponse.voiceResponse.push(data["result"]["fulfillment"]["speech"]);
            resolve(botResponse);
        }
        else {
            var locationIDs = cities.getLocationID(data["result"]["parameters"]["geo-city"][0]);
            dataAPI.generalAPICall({ "cmd_id": "get_warehouse" }, token)
                .then(locationData => {
                    var locationData2 = [];
                    Object.keys(locationData).forEach(val => {
                        locationData2 = locationData2.concat(locationData[val].locations)
                    });
                    var alltickets = locationData2.filter(a => {
                        for (let i = 0; i < locationIDs.length; i++) {
                            if ((a.location_id) == locationIDs[i])
                                return a
                        }
                    })
                    var alltickets2 = alltickets.map(a => { if (a.ticket_numbers) return (a.ticket_numbers) }).filter(a => { if (a) return a });
                    var mergedAlltickets2 = [].concat.apply([], alltickets2);
                    if (mergedAlltickets2.length <= 0) {
                        botResponse.textResponse.push(`No details found`);
                        botResponse.voiceResponse = botResponse.textResponse;
                        resolve(botResponse);
                    }
                    else {
                        var ticket_numbers = [];
                        ticket_numbers.push(mergedAlltickets2[0]);
                        dataAPI.generalAPICall({ "cmd_id": "get_details", "ticket_numbers": ticket_numbers }, token)
                            .then(allTicketsData => {
                                if (allTicketsData[0]) {
                                    var outagewindow = ``;
                                    allTicketsData[0].forEach(a => {
                                        if (a.header == "Outage Window From") {
                                            var fromDate = a.value.text != "" ? new Date(a.value.text).toDateString() : "";
                                            outagewindow += `The outage window is from "${fromDate}" to `
                                        }
                                        if (a.header == "Outage Window To") {
                                            var toDate = a.value.text != "" ? new Date(a.value.text).toDateString() : "";
                                            outagewindow += `"${toDate}"`
                                        }
                                    });
                                    botResponse.textResponse.push(outagewindow);
                                    botResponse.voiceResponse = botResponse.textResponse;
                                    resolve(botResponse);
                                }
                                else {
                                    botResponse.textResponse.push(`No details found`);
                                    botResponse.voiceResponse = botResponse.textResponse;
                                    resolve(botResponse);
                                }
                            }).catch(err => {
                                reject(err);
                            });
                    }
                }).catch(err => {
                    reject(err);
                });
        }
    })
    var final = await result;
    return final;
}

function suggestionsValues(userRole) {
    if (utils.checkPrivilegedUser(userRole)) {
        return suggestion_cxo
    }
    else {
        return suggestion_noncxo
    }
}

module.exports = intentHandlers;