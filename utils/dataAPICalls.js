// const apis = {}
// var request = require('request-promise');
// const url = require("../config").dataAPIURL

// apis.generalAPICall = async (body) => {
//     const promise = new Promise((resolve, reject) => {
//         request({
//             "method": "POST",
//             "uri": url,
//             "json": true,
//             "headers": {
//                 "Content-Type": "application/json"
//             },
//             "body": body
//         }).then(function (response) {
//             resolve(response);
//         }).catch(err => {
//             reject(err);
//         })
//     })
//     var result = await promise;
//     return result;
// }

// apis.getCloudServicesSummary = async (services,type) => {
//     var finalData = [];
//     for (let i = 0; i < services.length; i++) {
//         var body = { "cmd_id": "get_summary", "cloud_service": services[i], "chart_type": type }
//         const promise = new Promise((resolve, reject) => {
//             request({
//                 "method": "POST",
//                 "uri": url,
//                 "json": true,
//                 "headers": {
//                     "Content-Type": "application/json"
//                 },
//                 "body": body
//             }).then(function (response) {
//                 resolve({
//                     "type": services[i],
//                     "data": response
//                 });
//             }).catch(err => {
//                 reject(err);
//             })
//         })
//         var result = await promise;
//         finalData.push(result);
//     }
//     return finalData;
// }

// apis.getcloudServicesTopSpent = async (services, topSpent) => {
//     var finalData = [];
//     for (let j = 0; j < topSpent.length; j++) {
//         for (let i = 0; i < services.length; i++) {
//             const promise = new Promise((resolve, reject) => {
//                 var body = { "cmd_id": "get_summary", "cloud_service": services[i], "chart_type": topSpent[j] }
//                 request({
//                     "method": "POST",
//                     "uri": url,
//                     "json": true,
//                     "headers": {
//                         "Content-Type": "application/json"
//                     },
//                     "body": body
//                 }).then(function (response) {
//                     resolve({
//                         "serviceType": services[i],
//                         "dataType": topSpent[j],
//                         "data": response
//                     });
//                 }).catch(err => {
//                     reject(err);
//                 })
//             })
//             var result = await promise;
//             finalData.push(result);
//         }
//     }
//     return finalData;
// }

// module.exports = apis