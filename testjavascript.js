// This JS enable python code to pass body parameters to the BOT and get respective responses

var jsondata    = JSON.parse(decodeURIComponent(process.argv[2]));

const chatbotController = require('./controllers/chatbotController');

chatbotController.getDashboardData(jsondata).then(data => {
    var finalRes = {message: data, statusCode: 200};
	console.log(JSON.stringify(finalRes));
}).catch(err => {
    var finalRes = {message: err, statusCode: 500};
	console.log(JSON.stringify(finalRes));         
});