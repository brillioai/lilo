const utils = {};
var apiai = require('apiai');
var config = require('../config');
var app = apiai(config.nlpKey);
//   Dialog API Call

utils.apiCall = async (userUtterance, sessionID) => {
    var a = userUtterance;
    var result = new Promise((resolve, reject) => {
        var request = app.textRequest(a, {
            sessionId: sessionID
        });
        request.on('response', function (response) {
            resolve(response);
        });
        request.on('error', function (error) {
            reject(error);
        });
        request.end();
    });
    var final = await result;
    return final;
}

utils.greet = () => {
    var myDate = new Date();
    var hrs = myDate.getHours();
    var greet;
    if (hrs < 12)
        greet = 'Good Morning';
    else if (hrs >= 12 && hrs < 17)
        greet = 'Good Afternoon';
    else if (hrs >= 17 && hrs <= 24)
        greet = 'Good Evening';
    return greet
}
utils.getMonth = (num) => {
    return [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"][num]

}

utils.checkPrivilegedUser = (user) => {
    var found = config.privilegedUserGroup.find(function (element) {
        return element.toLowerCase() == user.toLowerCase();
    });
    if (found) {
        return true;
    }
    else {
        return false;
    }
}

module.exports = utils;
