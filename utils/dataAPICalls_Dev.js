const apis = {}
var request = require('request-promise');
const url = require("../config").dataAPIURL

apis.generalAPICall = async (body, token) => {
    const promise = new Promise((resolve, reject) => {
        request({
            "method": "POST",
            "uri": url,
            "json": true,
            "headers": {
                "Content-Type": "application/json",
                "Authorization": token
            },
            "body": body
        }).then(function (response) {
            resolve(response);
        }).catch(err => {
            reject(err)
        })
    })
    var result = await promise;
    return result;
}

apis.getCloudServicesSummary = async (services, type, token) => {
    var finalData = [];
    for (let i = 0; i < services.length; i++) {
        var body = { "cmd_id": "get_summary", "cloud_service": services[i], "chart_type": type }
        const promise = new Promise((resolve, reject) => {
            request({
                "method": "POST",
                "uri": url,
                "json": true,
                "headers": {
                    "Content-Type": "application/json",
                    "Authorization": token
                },
                "body": body
            }).then(function (response) {
                resolve({
                    "type": services[i],
                    "data": response
                });
            }).catch(err => {
                reject(err);
            })
        })
        var result = await promise;
        finalData.push(result);
    }
    return finalData;
}

apis.getcloudServicesTopSpent = async (services, topSpent, token) => {
    var finalData = [];
    for (let j = 0; j < topSpent.length; j++) {
        for (let i = 0; i < services.length; i++) {
            const promise = new Promise((resolve, reject) => {
                var body = { "cmd_id": "get_summary", "cloud_service": services[i], "chart_type": topSpent[j] }
                request({
                    "method": "POST",
                    "uri": url,
                    "json": true,
                    "headers": {
                        "Content-Type": "application/json",
                        "Authorization": token
                    },
                    "body": body
                }).then(function (response) {
                    resolve({
                        "serviceType": services[i],
                        "dataType": topSpent[j],
                        "data": response
                    });
                }).catch(err => {
                    reject(err);
                })
            })
            var result = await promise;
            finalData.push(result);
        }
    }
    return finalData;
}


apis.generalAPICallForCost = async (service_type, chart_type , token) => {
    var finalData = [];
    for (let i = 0; i < service_type.length; i++) {
        var body = { "cmd_id": "get_cost_details", "cloud_service": service_type[i], "chart_type": chart_type }
        const promise = new Promise((resolve, reject) => {
            request({
                "method": "POST",
                "uri": url,
                "json": true,
                "headers": {
                    "Content-Type": "application/json",
                    "Authorization": token
                },
                "body": body
            }).then(function (response) {
                resolve(response);
            }).catch(err => {
                reject(err)
            })
        })
        var result = await promise;
        if(chart_type == "cost_by_resource_group") {finalData.push(result);}
        else {    finalData.push(result[0]);}
    }
    return finalData;
}

module.exports = apis